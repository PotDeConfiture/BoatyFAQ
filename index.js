// Importations
const Discord = require("discord.js");
const client = new Discord.Client()
const fs = require("fs");
const config = require("./config.json")
const checkUpdate = require('check-update-github');
const pkg = require('./package.json');


// JSON
    // Get content from file
    var contents = fs.readFileSync("config.json");
    // Define to JSON type
    var jsonContent = JSON.parse(contents);

const prefix = jsonContent.prefix

// Quand le bot est connecté
client.on('ready', function () {
    console.log(`\nLogged as ${client.user.tag} (${client.user.id}) on ${client.guilds.size} server(s) \n`)
    client.user.setActivity(`sa FAQ`, { type: "WATCHING"})
    // Check for update
    checkUpdate({
        name: pkg.name, 
        currentVersion: pkg.version, 
        user: 'PotDeConfitures',
        branch: 'master'
        }, function(err, latestVersion, defaultMessage){
        if(!err){
            console.log(defaultMessage);
        }
    });
    })

    client.on('message', async message => {
        if(message.author.bot) return;
      

        if(message.content.startsWith(prefix + "setup"))
            {
                message.delete();
                if(!message.member.hasPermission('ADMINISTRATOR')) return; // Checks if the user has the permission
                var channelid = {
                    prefix: jsonContent.prefix,
                    token: jsonContent.token,
                   channelid: message.channel.id
                
                    }
                    
                
                const FAQEmbed = new Discord.RichEmbed()
                    .setTitle(`FAQ de ${message.guild.name}`)
                        // Les questions
                        .addField(`Comment inviter Boaty ?`, "Tapez **1**")
                        .addField(`Comment je peux aider le serveur et le bot ?`, `Tapez **2**`)
                        .addField(`Comment je peux avoir la liste des commandes ?`, "Tapez **3**")
                        .addField('J\'ai trouvé un bug ! J\'en fais quoi ?', 'Tapez **4**')
                        .addField('Qui est le fondateur du serveur ?', 'Tapez **5**')
                        .addField('Je souhaite vous aidez mais j\'ai la flemme de rédiger une pub', 'Taper **6**')
                        .addField('J\'ai une suggestion ! Mais je la poste où ?', 'Tapez **7**')
                        .addField('Comment je peux accéder aux channels de pubs ?', 'Tapez **8**')
                        .addField('J\'ai lu toute la FAQ mais ma question est toujours sans réponse...', 'Tapez **9**')
                    .setColor("#FF0000")
                    .setFooter("La réponse sera envoyée en message privé")
                    .setTimestamp()
                        fs.writeFile("./config.json", JSON.stringify(channelid), (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            };
                            console.log("File has been modified. Please restart the bot");
                        });
                    message.channel.send(FAQEmbed)
            }
            if(message.channel.id === jsonContent.channelid)
                {
                    var i=message.content;
                    var answer = "";
                    switch (i) {
                        case '1':
                          console.log(1);
                          message.delete()
                          answer = "Vous pouvez inviter Boaty avec ce lien :\rhttps://discordapp.com/oauth2/authorize?client_id=555076841652748308&permissions=0&redirect_uri=https%3A%2F%2Fdiscord.gg%2FAzNVf9U&scope=bot"
                          break;
                        case '2':
                          console.log(2);
                          answer = "Vous pouvez nous aider en :\rVotant sur https://discordbots.org/bot/555076841652748308 pour avoir plus de visibilité sur ce site et inviter des amis à utiliser le bot et à rejoindre le support."
                          message.delete();
                          break;
                        case '3':
                          console.log(3);
                          answer = "Faites la commande bo!help pour avoir la liste des commandes"
                          message.delete();
                          break;
                        case '4':
                          console.log(4);
                          answer = "Si vous avez trouvé un bug :\r Envoyez un mp à `@PotDeConfiture#4886`\r**NE LE PARTAGEZ À PERSONNE D'AUTRE !**"
                          message.delete();
                          break;
                        case '5':
                          console.log(5);
                          answer = "Le papa de Boaty est `@PotDeConfiture#4886`"
                          message.delete();
                          break;
                        case '6':
                          console.log(6);
                         answer = "Vous pouvez trouver notre pub dans le #cahne"
                          message.delete();
                        break;
                          case '7':
                          console.log(7);
                          answer = "Vous pouvez la poster dans le #『👨🏻💻』suggestions"
                          message.delete();
                          break;
                        case '8':
                          console.log(8);
                          answer = "Vous devez avoir invité 5 amis pour poster dans le #『🥈』pub-12h et 10 pour le #『🥇』pub-4h"
                          message.delete();
                          break;
                        case '9':
                          console.log(9);
                          answer = "Si vous n'avez pas trouvé votre réponse, merci de bien vouloir la poser dans le #『❓』aide "
                          message.delete();
                          break;
                        default:
                          return message.delete();
                      }
                    const AnswerEmbed = new Discord.RichEmbed()
                      .setTitle("La réponse à votre question est là !")
                      .setDescription(answer)
                      .setFooter('Bonne chance !')
                      .setColor("#7CFC00")
                      .setTimestamp()
                    message.author.send(AnswerEmbed).catch(console.error);
                }

    })

client.login(jsonContent.token)